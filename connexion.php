<?php

session_start();

$bdd = new PDO('mysql:host=127.0.0.1;port=8889;dbname=vinci', 'root', 'root');


if(isset($_POST['valider'])) {

    if(!empty($_POST['identifiant']) AND !empty($_POST['password'])) {

        $verif_identifiant = $bdd->prepare('SELECT * FROM utilisateurs WHERE identifiant = ?');
        $verif_identifiant->execute(array($_POST['identifiant']));
        $utilisateur = $verif_identifiant->fetch();
        $identifiant_exist = $verif_identifiant->rowCount();

        if($identifiant_exist === 1) {

            if(password_verify($_POST['password'], $utilisateur['mot_de_passe'])) {

                $_SESSION['id'] = $utilisateur['id'];

                header('Location: espace_employe.php');

            } else {
                echo "mauvais password";
            }

        }
    }

}

if(empty($_SESSION['id'])){

?>

    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>VINCI-Connexion</title>
    </head>
    <body>
        <div>
            <form method="POST">
                <input type="text" placeholder="Identifiant" name="identifiant">
                <input type="password" placeholder="Mot de passe" name="password">
                <input type="submit" value="Valider" name="valider">
            </form>
        </div>
    </body>
    </html>

<?php
}
?>